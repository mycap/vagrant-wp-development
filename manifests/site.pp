node default {

  class {'apache':  }
  class {'apache::mod::php': }
  apache::mod { 'rewrite': }

  apache::vhost { 'www.flexpay.se':
    priority        => '10',
    vhost_name      => '*',
    port            => '80',
    docroot         => '/var/www',
    serveraliases   => [ 'flexpay.se' ],
    custom_array    => [
      'DirectoryIndex index.php index.html',
      'AddHandler php5-script .php',
      'AddType text/html .php'
    ],
  }

  apache::vhost { 'default':
    ensure          => absent,
    priority        => '00',
    port            => '80',
    docroot         => '/var/www',
  }

  class { 'mysql': }
  class { 'mysql::php': }

  class { 'mysql::server':
    config_hash => {
      'root_password' => 'webadmin-mysql',
      'bind_address'  => '0.0.0.0'
    }
  }

  mysql::db { 'wordpress':
    user     => 'webuser',
    password => 'webadmin-mysql',
    host     => '%',
    grant    => ['all'],
  }

  file { '/var/www/index.html':
    ensure  => present,
    content => "<h1>Success!</h1><p>See <a href=\"system-phpinfo.php\">PHP info</a> if system is successful.</p><p>Local IP: <b>$::ipaddress_eth2</b></p>",
    owner   => 'vagrant',
    group   => 'vagrant',
    mode    => '0644',
    require => Class['apache::mod::php'],
  }

  file { '/var/www/system-phpinfo.php':
    ensure  => present,
    content => '<?php phpinfo(); ?>',
    owner   => 'vagrant',
    group   => 'vagrant',
    mode    => '0644',
    require => Class['apache::mod::php'],
  }

  file { '/var/www':
    ensure  => directory,
    owner   => 'vagrant',
    group   => 'vagrant',
    mode    => '0777',
    recurse => true,
  }

}
