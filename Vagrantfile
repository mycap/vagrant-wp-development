# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant::Config.run do |config|
  # All Vagrant configuration is done here. The most common configuration
  # options are documented and commented below. For a complete reference,
  # please see the online documentation at vagrantup.com.

  # This can be set to the host name you wish the guest machine to have.
  config.vm.host_name = "wordpress.example.com"

  # Every Vagrant virtual environment requires a box to build off of.
  #config.vm.box = "quantal-server-cloudimage-amd64"
  config.vm.box = "base"

  # The url from where the 'config.vm.box' box will be fetched if it
  # doesn't already exist on the user's system.
  #config.vm.box_url = "http://cloud-images.ubuntu.com/quantal/current/quantal-server-cloudimg-vagrant-amd64-disk1.box"
  config.vm.box_url = "http://files.vagrantup.com/precise64.box"

  # Boot with a GUI so you can see the screen. (Default is headless)
  #config.vm.boot_mode = :gui

  # Assign this VM to a host-only network IP, allowing you to access it
  # via the IP. Host-only networks can talk to the host machine as well as
  # any other machines on the same network, but cannot be accessed (through this
  # network interface) by any external networks.
  config.vm.network :hostonly, "192.168.62.100"

  # Assign this VM to a bridged network, allowing you to connect directly to a
  # network using the host's network device. This makes the VM appear as another
  # physical device on your network.
  config.vm.network :bridged

  # Forward a port from the guest to the host, which allows for outside
  # computers to access the VM, whereas host only networking does not.
  # config.vm.forward_port 80, 8080

  # Share an additional folder to the guest VM. The first argument is
  # an identifier, the second is the path on the guest to mount the
  # folder, and the third is the path on the host to the actual folder.
  # config.vm.share_folder "v-data", "/vagrant_data", "../data"

  # Enabling DNS proxy in NAT mode.
  # The NAT engine by default offers the same DNS servers to the guest
  # that are configured on the host.
  config.vm.customize ["modifyvm", :id, "--natdnsproxy1", "on"]

  # Using the host's resolver as a DNS proxy in NAT mode.
  # For resolving network names, the DHCP server of the NAT engine offers a
  # list of registered DNS servers of the host. If for some reason you need
  # to hide this DNS server list and use the host's resolver settings,
  # thereby forcing the VirtualBox NAT engine to intercept DNS requests and
  # forward them to host's resolver
  config.vm.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]

  # Enable provisioning with shell scripts. We need to fix the resolv.conf
  # file to be able to properly run Puppet.
  config.vm.provision :shell do |shell|
    shell.inline = 'echo "nameserver 8.8.8.8" > /etc/resolv.conf'
    shell.inline = 'echo "nameserver 8.8.4.4" >> /etc/resolv.conf'
    shell.inline = 'echo "search example.com"  >> /etc/resolv.conf'
    shell.inline = 'apt-get update'
  end

  # Enable provisioning with Puppet stand alone.  Puppet manifests
  # are contained in a directory path relative to this Vagrantfile.
  # You will need to create the manifests directory and a manifest in
  # the file base.pp in the manifests_path directory.
  #
  config.vm.provision :puppet do |puppet|
    puppet.manifests_path = "manifests"
    puppet.module_path    = "modules"
    puppet.manifest_file  = "site.pp"
  end

end
