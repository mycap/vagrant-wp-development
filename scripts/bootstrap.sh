#!/bin/bash

# Fix resolver issues
echo "nameserver 8.8.8.8" > /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf
echo "search example.com"  >> /etc/resolv.conf

# Run apt-get update
apt-get update
