# Wordpress Development Environment

## Installation

Installera [Virtualbox](https://www.virtualbox.org/) och [Vagrant](http://www.vagrantup.com/).

Kör sedan följande i kommandoraden, efter att ha gått in i vagrant-mappen (oftast `vagrant-wp-development`):

    vagrant up

Nu skall det vara möjlgt att nå utvecklingsmiljön via IP 192.168.62.100.

En address som kan användas av andra i det lokala nätverket finns på websidan för [192.168.62.100](http://192.168.62.100).

För att ladda upp filer med mera, använd en FTP-klient som stödjer SFTP och använd följande uppgifter:

    Användarnamn: vagrant
    Lösenord:     vagrant
    Port:         22
    Host:         192.168.62.100

Gå till `/var/www` och ladda upp alla filer där.

MySQL uppgifter:

    Användarnamn: webuser
    Lösenord:     webadmin-mysql
    Databas:      wordpress
    Port:         3306
    Host:         192.168.62.100

För att starta om någon av tjänsterna i utvecklingsmiljön, använd en SSH-klient med användaruppgifterna:

    Användarnamn: vagrant
    Lösenord:     vagrant
    Port:         22
    Host:         192.168.62.100

Starta om genom att köra:

    sudo /etc/init.d/apache2 restart
    sudo /etc/init.d/mysql restart

När du är klar med utvecklandet, kör: `vagrant destroy`
